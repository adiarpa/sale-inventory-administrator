/* Populate tabla clients */
INSERT INTO clients (name,last_name,document,document_type,address,phones,city,eliminated,email) VALUES('Adiel','Arguelles','123456789','CC','Calle 3','3114325678','Santa Marta',false,'adiarpa@gmail.com');
INSERT INTO clients (name,last_name,document,document_type,address,phones,city,eliminated,email) VALUES('Marisol','Arenillas','123456789','CC','Calle 22','3134325678','Barranquilla',false,'marisol@gmail.com');
INSERT INTO clients (name,last_name,document,document_type,address,phones,city,eliminated,email) VALUES('Salome','Arguelles','123456789','RC','Calle 43','3114325678','San Sebastian',false,'salo@gmail.com');

/* Populate tabla articles */
INSERT INTO articles (name, brand, category, description , price , enabled , quantity) VALUES('Radio para Bicicleta N 26', null, 'Bicicleta', 'Radio bicicleta',3246,true,10);
INSERT INTO articles (name, brand, category, description , price , enabled , quantity) VALUES('LLanta Bicicleta N 26', null, 'Bicicleta', 'Llanta bicicleta',3246,true,10);

/* Populate tabla tickets */
INSERT INTO tickets (creationdate,description,observation,total,client_id) VALUES('2023-06-28 12:44:35.0','Factura Llanta','Compra de llanta de moto',102550,1);
INSERT INTO tickets (creationdate,description,observation,total,client_id) VALUES('2023-06-28 14:44:35.0','Factura Bicicleta','Compra de Bicicleta',350000,1);

/* Populate tabla ticketitems */
INSERT INTO ticketitems (creationdate, quantity, article_id, ticket_id) VALUES('2023-06-25 22:44:35.0', 2,1,1);

