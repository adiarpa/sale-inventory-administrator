package com.a2p.software.sales.inventory.administrator.domain;

import com.a2p.software.sales.inventory.administrator.api.dto.Articles;
import com.a2p.software.sales.inventory.administrator.api.dto.Clients;
import java.util.List;

public interface RepositoryManager {

    /***
     * Students Repository *
     */
    default List<Clients> findClientesAll(){return null;}
    default Clients getClienteId(Long id){return null;}
    default void clienteSave(Clients client){}
    default Clients updateCliente(Clients client){return null;}

    /***
     * Articles Repository *
     */
    default List<Articles> findArticlesAll(){return null;}
    default List<Articles> findArticlesAllDisabled(){return null;}
    default Articles getArticleId(Long id){return null;}
    default void articleSave(Articles article){}
    default Articles updateArticle(Articles article){return null;}

}
