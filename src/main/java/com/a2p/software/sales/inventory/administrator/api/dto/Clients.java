package com.a2p.software.sales.inventory.administrator.api.dto;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "clients")
public class Clients implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Column(name = "last_name")
    private String lastName;
    private String document;

    @Column(name = "document_type")
    private String documentType;
    private String address;
    private String phones;
    private String city;
    private boolean eliminated;
    private String email;

/*    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Tickets> tickets;

    public Clients() {
        tickets = new ArrayList<>();
    }

    public void addTicket(Tickets ticket){
        tickets.add(ticket);
    }*/

}
