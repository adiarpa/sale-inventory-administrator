package com.a2p.software.sales.inventory.administrator.services;

import com.a2p.software.sales.inventory.administrator.api.dto.Articles;
import com.a2p.software.sales.inventory.administrator.api.dto.Clients;
import com.a2p.software.sales.inventory.administrator.domain.RepositoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ManagerServices {

    @Autowired
    private RepositoryManager repositoryManager;

    public List<Clients> findClientsAll(){
        return repositoryManager.findClientesAll();
    }

    public Clients getClientId(Long id){
        return repositoryManager.getClienteId(id);
    }

    public void clientSave(Clients client) {
        repositoryManager.clienteSave(client);
    }

    public Clients updateClient(Clients client){
        return repositoryManager.updateCliente(client);
    }

    public List<Articles> findArticlesAll(){
        return repositoryManager.findArticlesAll();
    }

    public List<Articles> findArticlesAllDisabled(){
        return repositoryManager.findArticlesAllDisabled();
    }

    public Articles getArticleId(Long id){
        return repositoryManager.getArticleId(id);
    }

    public void articleSave(Articles article) {
        repositoryManager.articleSave(article);
    }

    public Articles updateArticle(Articles article){
        return repositoryManager.updateArticle(article);
    }

}
