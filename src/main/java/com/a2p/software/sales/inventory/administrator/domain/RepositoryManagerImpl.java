package com.a2p.software.sales.inventory.administrator.domain;

import com.a2p.software.sales.inventory.administrator.api.dto.Articles;
import com.a2p.software.sales.inventory.administrator.api.dto.Clients;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public class RepositoryManagerImpl implements RepositoryManager {

    @PersistenceContext
    EntityManager entityManager;

    /***
     * Clients Repository Implement *
     */
    @Override
    @Transactional
    public List<Clients> findClientesAll() {
        String query = "FROM Clients c where c.eliminated=false";
        return (List<Clients>) entityManager.createQuery(query)
                .setMaxResults(100)
                .getResultList();
    }

    @Override
    @Transactional
    public Clients getClienteId(Long id) {
        return entityManager.find(Clients.class, id);
    }

    @Override
    @Transactional
    public void clienteSave(Clients client) {
        entityManager.merge(client);
    }

    @Transactional
    public Clients updateCliente(Clients client) {
        return entityManager.merge(client);
    }

    /***
     * Articles Repository Implement *
     */
    @Override
    @Transactional
    public List<Articles> findArticlesAll() {
        String query = "FROM Articles a where a.enabled=true";
        return (List<Articles>) entityManager.createQuery(query)
                .setMaxResults(100)
                .getResultList();
    }

    @Override
    @Transactional
    public List<Articles> findArticlesAllDisabled() {
        String query = "FROM Articles a where a.enabled=false";
        return (List<Articles>) entityManager.createQuery(query)
                .setMaxResults(100)
                .getResultList();
    }

    @Override
    @Transactional
    public Articles getArticleId(Long id) {
        return entityManager.find(Articles.class, id);
    }

    @Override
    @Transactional
    public void articleSave(Articles article) {
        entityManager.merge(article);
    }

    @Transactional
    public Articles updateArticle(Articles article) {
        return entityManager.merge(article);
    }

}
