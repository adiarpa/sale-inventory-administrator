package com.a2p.software.sales.inventory.administrator.repository;

import com.a2p.software.sales.inventory.administrator.api.dto.Tickets;
import org.springframework.data.repository.CrudRepository;

public interface TicketRepository extends CrudRepository<Tickets,Long> {

}
