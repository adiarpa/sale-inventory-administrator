package com.a2p.software.sales.inventory.administrator.api.controllers;

import com.a2p.software.sales.inventory.administrator.api.dto.Articles;
import com.a2p.software.sales.inventory.administrator.api.dto.Clients;
import com.a2p.software.sales.inventory.administrator.services.ManagerServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/section")
@CrossOrigin(origins = "http://localhost:4200")
public class ApiRestController {

    @Autowired
    private ManagerServices managerServices;

    /***
     * Controller Clients
     */
    @GetMapping("/clients")
    public List<Clients> findClientsAll(){
        return managerServices.findClientsAll();
    }

    @GetMapping("/clients/{id}")
    public Clients getClientId(@PathVariable Long id){
        return managerServices.getClientId(id);
    }

    @PostMapping("/clients")
    public void clientSave(@RequestBody Clients client){
        managerServices.clientSave(client);
    }

    @DeleteMapping("/clients/{id}")
    public ResponseEntity<?> deleteClient(@PathVariable Long id){
        Clients client = managerServices.getClientId(id);
        if (client != null && !client.isEliminated()) {
            client.setEliminated(true);
            managerServices.clientSave(client);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/clients/{id}")
    public ResponseEntity<Clients> updateClient(@RequestBody Clients client, @PathVariable Long id){
        Clients currentClient = this.managerServices.getClientId(id);
        if (currentClient != null) {
            currentClient.setName(client.getName());
            currentClient.setLastName(client.getLastName());
            currentClient.setDocument(client.getDocument());
            currentClient.setDocumentType(client.getDocumentType());
            currentClient.setAddress(client.getAddress());
            currentClient.setPhones(client.getPhones());
            currentClient.setCity(client.getCity());
            currentClient.setEliminated(client.isEliminated());
            currentClient.setEmail(client.getEmail());

            this.managerServices.clientSave(currentClient);
            return ResponseEntity.status(HttpStatus.CREATED).body(currentClient);
        }
        return ResponseEntity.notFound().build();
    }

    /***
     * Controller Articles
     */
    @GetMapping("/articles")
    public List<Articles> findArticlesAll(){
        return managerServices.findArticlesAll();
    }

    @GetMapping("/articlesDisabled")
    public List<Articles> findArticlesAllDisabled(){
        return managerServices.findArticlesAllDisabled();
    }

    @GetMapping("/articles/{id}")
    public Articles getArticleId(@PathVariable Long id){
        return managerServices.getArticleId(id);
    }

    @PostMapping("/articles")
    public void articleSave(@RequestBody Articles article){
        article.setEnabled(true);
        managerServices.articleSave(article);
    }

    @DeleteMapping("/articles/{id}")
    public ResponseEntity<?> deleteArticle(@PathVariable Long id){
        Articles article = managerServices.getArticleId(id);
        if (article != null && !article.isEnabled()) {
            article.setEnabled(false);
            managerServices.articleSave(article);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/articles/{id}")
    public ResponseEntity<Articles> updateArticle(@RequestBody Articles article, @PathVariable Long id){
        Articles currentArticle = this.managerServices.getArticleId(id);
        if (currentArticle != null) {
            currentArticle.setBrand(article.getBrand());
            currentArticle.setDescription(article.getDescription());
            currentArticle.setPrice(article.getPrice());
            currentArticle.setEnabled(article.isEnabled());
            currentArticle.setQuantity(article.getQuantity());
            this.managerServices.articleSave(currentArticle);
            return ResponseEntity.status(HttpStatus.CREATED).body(currentArticle);
        }
        return ResponseEntity.notFound().build();
    }

}
