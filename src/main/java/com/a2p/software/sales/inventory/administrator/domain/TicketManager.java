package com.a2p.software.sales.inventory.administrator.domain;

import com.a2p.software.sales.inventory.administrator.api.dto.Tickets;

import java.util.List;


public interface TicketManager {

    List<Tickets> findTicketsAll();
    Tickets findTicketById(Long id);
    void ticketCreate(Tickets ticket);
    Tickets updateTicket(Tickets ticket);
    void deleteTicket(Long id);

}
