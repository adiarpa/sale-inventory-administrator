package com.a2p.software.sales.inventory.administrator.api.controllers;

import com.a2p.software.sales.inventory.administrator.api.dto.Tickets;
import com.a2p.software.sales.inventory.administrator.domain.TicketManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/ticket")
public class TicketController {

    @Autowired
    private TicketManager ticketManager;

    @GetMapping("/ver")
    public List<Tickets> findTicketsAll(){
        return ticketManager.findTicketsAll();
    }

    @GetMapping("/ver/{id}")
    public Tickets watchTicket(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash){
        return ticketManager.findTicketById(id);
    }

    @PostMapping("/create")
    public void ticketCreate(@RequestBody Tickets ticket){
        ticketManager.ticketCreate(ticket);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Tickets> updateTicket(@RequestBody Tickets ticket, @PathVariable Long id){
        Tickets currentTicket = ticketManager.findTicketById(id);
        if (currentTicket != null){
            currentTicket.setDescription(ticket.getDescription());
            currentTicket.setObservation(ticket.getObservation());
            currentTicket.setTotal(ticket.getTotal());
            currentTicket.setUpdatedDate(new Timestamp(new Date().getTime()));

            this.ticketManager.updateTicket(currentTicket);
            return ResponseEntity.status(HttpStatus.CREATED).body(currentTicket);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteTicket(@PathVariable Long id){
        Tickets ticket = ticketManager.findTicketById(id);
        if (ticket != null) {
            ticketManager.deleteTicket(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

}
