package com.a2p.software.sales.inventory.administrator.domain;

import com.a2p.software.sales.inventory.administrator.api.dto.Tickets;
import com.a2p.software.sales.inventory.administrator.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketManagerImpl implements TicketManager{

    @Autowired
    TicketRepository ticketRepository;

    @Override
    public List<Tickets> findTicketsAll() {
        return (List<Tickets>) ticketRepository.findAll();
    }

    @Override
    public Tickets findTicketById(Long id) {
        return ticketRepository.findById(id).orElse(null);
    }

    @Override
    public void ticketCreate(Tickets ticket) {
        ticketRepository.save(ticket);
    }

    @Override
    public Tickets updateTicket(Tickets ticket) {
        return ticketRepository.save(ticket);
    }

    @Override
    public void deleteTicket(Long id) {
        ticketRepository.deleteById(id);
    }

}
