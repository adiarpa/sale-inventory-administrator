package com.a2p.software.sales.inventory.administrator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalesInventoryAdministratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalesInventoryAdministratorApplication.class, args);
	}

}
